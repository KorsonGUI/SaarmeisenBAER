package sample;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import saarland.cispa.sopra.systemtests.AntInfo;

import java.util.Optional;

public class AntInfoPanel extends Node {


    private boolean food = false;
    private Integer id = -1;
    private String direction = "";
    private Integer pc = -1;
    private boolean[] register = new boolean[6];
    private Integer resttime = -1;
    private Character swarm = '&';
    private Boolean notPresent = false;
    private boolean empty;


    public AntInfoPanel(Optional<AntInfo> antOpt, boolean empty) {

        this.empty = empty;
        if (!antOpt.isPresent()) {
            this.notPresent = true;
        } else {
            AntInfo ant = antOpt.get();
            this.id = ant.getId();
            this.direction = ant.getDirection();
            this.pc = ant.getPc();
            this.register = ant.getRegister();
            this.resttime = ant.getRestTime();
            this.swarm = ant.getSwarm();
            this.food = ant.hasFood();
        }
    }

    public VBox getPanel() {
        HBox hbox = new HBox();
        hbox.setStyle("-fx-background-color: white");
        hbox.setPrefSize(280, 300);
        VBox outerVb = new VBox();
        HBox hb = new HBox();
        Text txt = new Text("Ant-Informations:");
        txt.setFont(Font.font("Impact"));
        txt.setStyle("-fx-font-size: 25");
        hb.getChildren().add(txt);
        hb.setStyle("-fx-background-color: white");
        hb.setAlignment(Pos.CENTER);
        WorldGUI.setBorderStandard(hb);
        outerVb.getChildren().add(hb);
        WorldGUI.setBorderStandard(outerVb);
        hbox.setAlignment(Pos.CENTER);
        if (empty) {
            outerVb.getChildren().add(hbox);
            return outerVb;
        }

        if (notPresent) {
            hbox.getChildren().add(new Label("No Ant on this Field"));
            outerVb.getChildren().add(hbox);
            return outerVb;
        }
//        hbox.getChildren().add(new Label(new StringBuilder().append("AntID: ").append(id).toString()));
//        hbox.getChildren().add(new Label(new StringBuilder().append("Current Direction: ").append(direction).toString()));
//        hbox.getChildren().add(new Label(new StringBuilder().append("AntPC: ").append(pc).toString()));
//        hbox.getChildren().add(new Label(new StringBuilder().append("Current Resttimer: ").append(resttime).toString()));
//        hbox.getChildren().add(new Label(new StringBuilder().append("Swarm: ").append(swarm).toString()));
//        hbox.getChildren().add(new Label(new StringBuilder().append("Carries Food: ").append(food).toString()));
//        hbox.getChildren().add(new Label(new StringBuilder().append("Register: ").append(makeReadableRegister(register)).toString()));

        VBox left = new VBox();
        VBox right = new VBox();
        left.setAlignment(Pos.TOP_CENTER);
        right.setAlignment(Pos.TOP_CENTER);
        getInfoPanel("ID: ", new StringBuilder().append(id).toString(), left, right);
        getInfoPanel("Current Direction:  ", new StringBuilder().append(direction).toString(), left, right);
        getInfoPanel("PC: ", new StringBuilder().append(pc).toString(), left, right);
        getInfoPanel("Current Resttimer: ", new StringBuilder().append(resttime).toString(), left, right);
        getInfoPanel("Swarm: ", new StringBuilder().append(swarm).toString(), left, right);
        getInfoPanel("Carries Food: ", new StringBuilder().append(food).toString(), left, right);
        getInfoPanel("Register: ", new StringBuilder().append(makeReadableRegister(register)).toString(), left, right);
        hbox.getChildren().add(left);
        hbox.getChildren().add(right);

        outerVb.getChildren().add(hbox);

        return outerVb;
    }

    private void getInfoPanel(String header, String val, VBox left, VBox right) {
        left.getChildren().add(new Label(header));
        right.getChildren().add(new Label(val));
    }

    private String makeReadableRegister(boolean[] register) {
        StringBuilder resBuilder = new StringBuilder();
        resBuilder.append('[');
        for (int i = 0; i < register.length; i++) {
            resBuilder.append(register[i] ? 1 : 0);
            if(i != register.length -1){
                resBuilder.append(", ");
            }
        }
        return resBuilder.append(']').toString();
    }
}
