package Implementations;


import saarland.cispa.sopra.systemtests.AntInfo;
import saarland.cispa.sopra.systemtests.FieldInfo;
import saarland.cispa.sopra.systemtests.WorldInfo;

import java.util.ArrayList;
import java.util.List;


public class MyWorld implements WorldInfo {

    private MapMatrix mM;
    private MapParser parser;


    public MyWorld(String Path) {
        this.parser = new MapParser(Path);
        MapMatrix mM = parser.getMap();
        MyField field = (MyField) mM.getFieldAt(1, 1);
        field.setAnt(new MyAnt(field));
        mM.setField(field);
        this.mM = mM;
    }

    @Override
    public int getHeight() {
        // TODO Auto-generated method stub
        return mM.height;
    }

    @Override
    public int getWidth() {
        // TODO Auto-generated method stub
        return mM.width;
    }

    @Override
    public FieldInfo getFieldAt(int x, int y) {
        // TODO Auto-generated method stub
        return mM.getFieldAt(x, y);
    }

    @Override
    public int getScore(char swarm) {
        if (swarm == 'A') {
            return 4;
        }
        if (swarm == 'B') {
            return 3;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public AntInfo getAnt(int id) {
        return null;
    }

    @Override
    public List<AntInfo> getAnts() {
        return new ArrayList<AntInfo>();
    }

}
