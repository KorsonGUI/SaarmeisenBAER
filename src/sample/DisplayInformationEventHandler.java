package sample;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import saarland.cispa.sopra.systemtests.AntInfo;
import saarland.cispa.sopra.systemtests.FieldInfo;
import saarland.cispa.sopra.systemtests.WorldInfo;

import java.util.Optional;

public class DisplayInformationEventHandler implements EventHandler<MouseEvent> {

    private final WorldGUI gui;
    private final int x;
    private final int y;


    public DisplayInformationEventHandler(WorldGUI gui, int x, int y) {
        this.gui = gui;
        this.x = x;
        this.y = y;
    }

    @Override
    public void handle(MouseEvent event) {
        gui.setCurrentX(x);
        gui.setCurrentY(y);
        gui.infoBox.getChildren().clear();
        gui.infoBox.getChildren().add(getFieldInformation(x, y));
        gui.infoBox.getChildren().add(getAntOnFieldInformation(x, y));
//        gui.updateInfos();
    }


    private Node getAntOnFieldInformation(int x, int y) {
        WorldInfo world = gui.getWorld();
        FieldInfo field = world.getFieldAt(x, y);
        Optional<AntInfo> ant = field.getAnt();
        AntInfoPanel antPanel = new AntInfoPanel(ant, false);
        return antPanel.getPanel();
    }

    private VBox getFieldInformation(int x, int y) {
        WorldInfo world = gui.getWorld();
        FieldInfo field = world.getFieldAt(x, y);
        FieldInfoPanel fieldPanel = new FieldInfoPanel(field, false);
        return fieldPanel.getPanel();
    }
}
