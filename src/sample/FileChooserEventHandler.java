package sample;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class FileChooserEventHandler implements EventHandler<MouseEvent> {
    ProtocolSetUpAssistant setUp;
    Stage mainStage;



    public FileChooserEventHandler(ProtocolSetUpAssistant protocolSetUpAssistant) {
    this.setUp = protocolSetUpAssistant;
    this.mainStage = protocolSetUpAssistant.getPrimaryStage();
    }

    @Override
    public void handle(MouseEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Text", "*.txt"));
        File selectedFile = fileChooser.showOpenDialog(this.mainStage);
        setUp.setProtocol(selectedFile);
    }
}
