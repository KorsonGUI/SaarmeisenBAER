package sample;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import saarland.cispa.sopra.controllers.Game;
import saarland.cispa.sopra.systemtests.GameInfo;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import saarland.cispa.sopra.controllers.Game;

public class GUIBuilder extends Application {


    private final String IconPath = "./resources/Icons/32x32.png";
    Map<Integer, File> brainMap = new HashMap<>();
    private GameInfo gameInfo;
    private File map = null;
    private File[] brains = null;
    private Stage primaryStage;
    private Long seed = null;
    private Text mapDisplay;
    private HBox brainDisplay;
    private String buttonStyleSheet = "-fx-text-fill: white;" + "-fx-font-family: Arial;" + "-fx-background-color: rgb(16,66,128);" + "-fx-border-color: black";
    private File resourcePath = new JFileChooser().getFileSystemView().getDefaultDirectory();
    private TextField seedInput;

//    public GUIBuilder(GameInfo gameInfo) {
//        this.gameInfo = new Game();
//    }

    static Node getlogo() {
        File file = new File("./resources/Icons/UniBar.png");
        Image image = new Image(file.toURI().toString());
        ImageView iv = new ImageView(image);
        iv.setFitWidth(560);
        iv.setPreserveRatio(true);
        return iv;
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.gameInfo = new Game();
        this.primaryStage = primaryStage;
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        hb.getChildren().add(getSetUpPanel());
        hb.getChildren().add(getLogoConfirmPanel());
        AnchorPane root = new AnchorPane();
        root.getChildren().add(hb);
        primaryStage.setScene(new Scene(root));
        try {
            primaryStage.getIcons().add(new Image(new FileInputStream(new File(IconPath))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        primaryStage.show();
    }

    private Node getLogoConfirmPanel() {
        VBox vb = new VBox();
        vb.setPrefSize(560, 720);
        vb.getChildren().add(getStartbutton());
        vb.getChildren().add(getLogoPanel());
        return vb;
    }

    private Node getLogoPanel() {
        return getlogo();
    }

    private Node getStartbutton() {
        Button btn = new Button();
        btn.setText("Start!");
        btn.setStyle(buttonStyleSheet + ";-fx-font-size: 120");
        btn.setPrefSize(1200, 1200);
        btn.setOnMouseClicked((MouseEvent e) -> {
            if (this.map == null) {
                throwMessagePopup("Please choose a valid MapFile");
            }
            if (this.brains == null) {
                throwMessagePopup("Please choose valid brains");
            }
            if (this.seed == null) {
                try {
                    handleSeed(seedInput);
                } catch (NumberFormatException ex) {
                    throwMessagePopup("Please enter a valid seed");
                }
            }
            if (this.map != null && this.brains != null && this.seed != null) {
                startGUI();
            }
        });
        return btn;
    }

    private void throwMessagePopup(String out) {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        VBox dialogVbox = new VBox(20);
        dialogVbox.setAlignment(Pos.CENTER);
        dialogVbox.setPrefSize(600, 300);
        Text txt = new Text(out);
        txt.setFont(Font.font("Arial"));
        txt.setStyle("-fx-font-size: 30");
        dialogVbox.getChildren().add(txt);
        Scene dialogScene = new Scene(dialogVbox, 600, 300);
        dialog.setScene(dialogScene);
//        dialog.getIcons().add(new Image(new FileInputStream(new File(WorldGUI.IconPath))));
        dialog.show();
    }

    private Node getSetUpPanel() {
        HBox hb = new HBox();
        VBox left = new VBox();
        VBox right = new VBox();
        left.setAlignment(Pos.CENTER_RIGHT);
        right.setAlignment(Pos.CENTER_LEFT);
        left.setPrefSize(280, 720);
        right.setPrefSize(280, 720);
        setChooserButtonBox(true, left, right);
        setChooserButtonBox(false, left, right);
        setSeedChooser(left, right);
        hb.setAlignment(Pos.CENTER);
        hb.setPrefSize(560, 720);
        hb.setStyle("-fx-background-color: white");
        hb.getChildren().add(left);
        hb.getChildren().add(right);
        return hb;
    }

    private void setChooserButtonBox(boolean b, VBox left, VBox right) {
        left.getChildren().add(getChooserButton(b));
        right.getChildren().add(b ? getChoiceDisplay(b) : initBrains());
        return;
    }

    private Node getBrainDisplay() {
        VBox vb = new VBox();
        vb.setAlignment(Pos.CENTER);
        for (Map.Entry<Integer, File> brain : brainMap.entrySet()) {
            vb.getChildren().add(getBrainMappingPanel(brain));
        }
        return vb;
    }

    private Node getBrainMappingPanel(Map.Entry<Integer, File> brain) {
        HBox hb = new HBox();
        hb.getChildren().add(new Text(brain.getValue().getName() + " as Brain number:    "));
        hb.getChildren().add(getResorterArea(brain));
        return hb;
    }

    private Node getResorterArea(Map.Entry<Integer, File> brain) {
        TextField field = new TextField();
        field.setMinWidth(30);
        field.setPrefWidth(30);
        field.setText(brain.getKey().toString());
        field.setOnKeyPressed((KeyEvent e) -> {
            if (e.getCode() == KeyCode.ENTER) {
                Integer i = 0;
                try {
                    i = Integer.parseInt(field.getText());
                } catch (NumberFormatException ex) {
                    try {
                        throwExceptionPopup(ex);
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }
                File temp = brainMap.get(i);
                brainMap.put(i, brain.getValue());
                brainMap.put(brain.getKey(), temp);
                updateBrainsDisplay();
            }
        });
        return field;
    }

    private Node getChoiceDisplay(boolean b) {
        Text txt = new Text();
        txt.setText(b ? "   Choose your battlefield..." : "     Choose your KI wisely...");
        if (b) {
            this.mapDisplay = txt;
        }
        return getHoriBox(txt);
    }

    private String getStringValBrains(File[] brains) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        for (File brain : brains) {
            stringBuilder.append(getStringVal(brain)).append("\n\n\n");
        }
        return stringBuilder.toString();

    }

    private String getStringVal(File map) throws IOException {
        return new String(Files.readAllBytes(map.toPath()), StandardCharsets.UTF_8);
    }

    private void setSeedChooser(VBox left, VBox right) {
        TextField textField = new TextField();
        textField.setPrefSize(140, 20);
        textField.setOnKeyPressed((KeyEvent e) -> {
            if (e.getCode() == KeyCode.ENTER) {
                handleSeed(seedInput);
            }
        });
        this.seedInput = textField;
        Button btn = new Button();
        btn.setPrefSize(180, 20);
        btn.setText("Confirm seed");
        btn.setStyle(buttonStyleSheet + "; -fx-font-size: 20");
        btn.setMinSize(280, 240);
        btn.setOnMouseClicked((MouseEvent e) -> {
            handleSeed(seedInput);
        });
        left.getChildren().add(getHoriBox(btn));
        right.getChildren().add(getHoriBox(textField));
        return;
    }

    private void handleSeed(TextField textField) {
        try {
            this.seed = Long.parseLong(textField.getText());
        } catch (NumberFormatException ex) {
            try {
                throwExceptionPopup(ex);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        }
    }

    private Node getHoriBox(Node child) {
        HBox hb = new HBox();
        hb.getChildren().add(child);
        hb.setPrefSize(280, 240);
        hb.setAlignment(Pos.CENTER);
        return hb;
    }

    private Node getChooserButton(boolean map) {
        Button btn = new Button();
        String text = map ? "Choose map file" : "Choose brains";
        btn.setText(text);
        btn.setStyle(buttonStyleSheet + "; -fx-font-size: 20");
        btn.setMinSize(280, 240);
        btn.setOnMouseClicked((MouseEvent e) -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle(text);
            fileChooser.setInitialDirectory(resourcePath);
            fileChooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter("txt", "*.txt"));

            if (map) {
                this.map = fileChooser.showOpenDialog(this.primaryStage);
                if (this.map != null) {
                    updateMapDisplay();
                    this.resourcePath = this.map.getParentFile().getParentFile();
                }

            } else {
                this.brains = getFileArrayFromList(fileChooser.showOpenMultipleDialog(this.primaryStage));
                if (this.brains != null) {
                    updateBrainsDisplayFromArray();
                    this.resourcePath = this.brains[0].getParentFile().getParentFile();
                }
            }
        });
        btn.setPrefSize(180, 20);
        return getHoriBox(btn);
    }

    private void updateBrainsDisplayFromArray() {
        brainMap = new HashMap<>();
        for (int i = 0; i < brains.length; i++) {
            brainMap.put(i, brains[i]);
        }
        updateBrainsDisplay();
    }

    private HBox initBrains() {
        this.brainDisplay = (HBox) getHoriBox(new Text("Choose some brains"));
        return brainDisplay;
    }

    private void updateBrainsDisplay() {
        this.brainDisplay.getChildren().clear();
        this.brainDisplay.getChildren().add(getBrainDisplay());
    }

    private void updateMapDisplay() {
        this.mapDisplay.setText(this.map.getName());
    }

    private File[] getFileArrayFromList(List<File> showOpenMultipleDialog) {
        if (showOpenMultipleDialog == null) {
            return null;
        } else {
            File[] arr = new File[showOpenMultipleDialog.size()];
            for (int i = 0; i < showOpenMultipleDialog.size(); i++)
                arr[i] = showOpenMultipleDialog.get(i);
            return arr;
        }
    }

    public void throwExceptionPopup(Exception e) throws FileNotFoundException {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        VBox dialogVbox = new VBox(20);
        TextArea ta = new TextArea(new StringBuilder().append(e.toString()).toString());
        ta.setPrefRowCount(20);
        dialogVbox.getChildren().add(ta);
        Scene dialogScene = new Scene(dialogVbox, 600, 300);
        dialog.setScene(dialogScene);
//        dialog.getIcons().add(new Image(new FileInputStream(new File(WorldGUI.IconPath))));
        dialog.show();
    }

    private void startGUI() {
//        try {
//            System.out.print(getStringVal(map.toPath));
//            System.out.print(getStringValBrains(brains));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        WorldGUI gui = new WorldGUI(this.gameInfo, seed, map, getBrainArray());
        try {
            gui.start(new Stage());
        } catch (Exception e) {
            try {
                throwExceptionPopup(e);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        }
    }

    private File[] getBrainArray() {
        File[] arr = new File[brainMap.values().size()];
        for (Map.Entry<Integer, File> brain : brainMap.entrySet()) {
            arr[brain.getKey()] = brain.getValue();
        }
        return arr;
    }
}
