package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class JumpToHardEventHandler implements EventHandler<ActionEvent> {

    private final int i;
    private final WorldGUI gui;

    public JumpToHardEventHandler(WorldGUI gui, int i) {
        this.i = i;
        this.gui = gui;
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            int nextStep = i;
            gui.setNextStep(nextStep);
            gui.update();
        } catch (NumberFormatException e) {
            gui.throwExceptionPopup(e);
        } catch (IllegalStateException e) {
            gui.throwEndOfSim();
        }
    }
}
