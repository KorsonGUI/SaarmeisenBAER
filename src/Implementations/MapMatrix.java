package Implementations;


import saarland.cispa.sopra.systemtests.FieldInfo;

import java.util.ArrayList;
import java.util.List;


public class MapMatrix {

    int width;
    int height;

    List<List<FieldInfo>> rows = new ArrayList<>();

    public MapMatrix(int width, int height){
        this.width = width; this.height = height;
        List<List<FieldInfo>> rows = new ArrayList<>(height);
        for (int y = 0; y < height; y++) {
            List<FieldInfo> row = new ArrayList<>(width);
            for (int x = 0; x < width; x++) {
                row.add(new MyField(x,y,"."));
            }
            rows.add(row);
        }
        this.rows = rows;
    }

    public FieldInfo getFieldAt(int x, int y) {
        return rows.get(y).get(x);
    }

    public void setField(MyField myField) {
        rows.get(myField.getY()).set(myField.getX(), myField);
    }
}
