package sample;


import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import saarland.cispa.sopra.systemtests.AntInfo;
import saarland.cispa.sopra.systemtests.FieldInfo;

import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

public class FieldInfoPanel extends Node {

    Map<Character, boolean[]> markers;
    Integer food;
    Optional<AntInfo> ant;
    Character type;
    Integer x;
    Integer y;
    boolean empty;


    public FieldInfoPanel(FieldInfo field, boolean empty) {
        this.markers = field.getMarkers();
        this.food = field.getFood();
        this.ant = field.getAnt();
        this.type = field.getType();
        this.x = field.getX();
        this.y = field.getY();
        this.empty = empty;
    }

    public VBox getPanel() {
        VBox outerVb = new VBox();
        HBox hb = new HBox();
        Text txt = new Text("Field-Informations:");
        txt.setFont(Font.font("Impact"));
        txt.setStyle("-fx-font-size: 25");
        hb.getChildren().add(txt);
        hb.setStyle("-fx-background-color: white");
        hb.setAlignment(Pos.CENTER);
        WorldGUI.setBorderStandard(hb);
        outerVb.getChildren().add(hb);
        WorldGUI.setBorderStandard(outerVb);
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER);
        if (!empty) {
            VBox left = new VBox();
            VBox right = new VBox();
            left.setAlignment(Pos.TOP_CENTER);
            right.setAlignment(Pos.TOP_CENTER);
            getInfoPanel("Foodcounter: ", new StringBuilder().append(food).toString(), left, right);
            getInfoPanel("Fieldtype: ", new StringBuilder().append(type).toString(), left, right);
            getInfoPanel("X-Coordinate: ", new StringBuilder().append(x).toString(), left, right);
            getInfoPanel("Y-Coordinate: ", new StringBuilder().append(y).toString(), left, right);
            getInfoPanel("Markers:\n", new StringBuilder().append(getMarkersReadable(markers)).toString(), left, right);
            hbox.getChildren().add(left);
            hbox.getChildren().add(right);
        }
        hbox.setPrefSize(280, 300);
        hbox.setStyle("-fx-background-color: white");
        outerVb.getChildren().add(hbox);
        return outerVb;
    }

    private void getInfoPanel(String header, String val, VBox left, VBox right) {
        left.getChildren().add(new Label(header));
        right.getChildren().add(new Label(val));
        return;
    }

    private String getMarkersReadable(Map<Character, boolean[]> markers) {
        StringBuilder sb = new StringBuilder();
        Iterator<Map.Entry<Character, boolean[]>> iter = markers.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<Character, boolean[]> entry = iter.next();
            sb.append(entry.getKey());
            sb.append("= ");
            sb.append(makeReadableRegister(entry.getValue()));
            if (iter.hasNext()) {
                sb.append(',').append('\n');
            }
        }
        return sb.toString();
    }


    private String makeReadableRegister(boolean[] register) {
        StringBuilder resBuilder = new StringBuilder();
        resBuilder.append('[');
        for (int i = 0; i < register.length; i++) {
            resBuilder.append(register[i] ? 1 : 0);
            if (i != register.length - 1) {
                resBuilder.append(", ");
            }
        }
        return resBuilder.append(']').toString();
    }
}
