package sample;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import parser.LogParser;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;

public class ProtocolSetUpAssistant extends Application {

    private File protocol;
    private Stage primaryStage;
    private final String IconPath = "./resources/Icons/32x32.png";
    private final String buttonStyleSheetPath = "./resources/Stylesheets/button.css";


    public ProtocolSetUpAssistant() {
    }

    public static void main(String[] args) {
        launch(args);
    }

    public File getProtocol() {
        return protocol;
    }

    public void setProtocol(File protocol) {
        this.protocol = protocol;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        VBox vb = new VBox();
        vb.setStyle("-fx-background-color: white");
        vb.getChildren().add(getTutorialBAER());
        vb.getChildren().add(getFileChooserButton());
        AnchorPane root = new AnchorPane();
        root.getChildren().add(vb);
        Scene myScene = new Scene(root);
        //TODO STYLESHEETS
//        myScene.getStylesheets().add(buttonStyleSheetPath);
        primaryStage.setScene(myScene);
//        primaryStage.getIcons().add(new Image(new FileInputStream(new File(IconPath))));
        primaryStage.setTitle("SaarmeisenBAER - Set Up Assistent");
        primaryStage.show();
    }

    private Node getFileChooserButton() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        hb.setStyle("-fx-background-color: white");
        Button btn = new Button();
        btn.setText("Choose Protocol File");
        btn.setFont(Font.font(WorldGUI.MAINFONT));
        btn.setOnMouseClicked((MouseEvent e) -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.setInitialDirectory(new JFileChooser().getFileSystemView().getDefaultDirectory());
            fileChooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter("JSON", "*.json"));
            File prot = fileChooser.showOpenDialog(this.primaryStage);
            this.protocol = prot;
            if (prot != null) {
                startGUI();
            }
        });
        btn.setPrefSize(180, 20);
        hb.getChildren().add(btn);
        return hb;
    }


    private void startGUI() {
        LogParser logParser = new LogParser();
        try {
            logParser.parseProtocol(protocol);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        WorldGUI gui = new WorldGUI(logParser, 123, new File(""), new File(""));
        try {
            gui.start(new Stage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Node getTutorialBAER() {
        File file = new File("./resources/Icons/UniBar.png");
        Image image = new Image(file.toURI().toString());
        ImageView iv = new ImageView(image);
        iv.setFitWidth(560);
        iv.setPreserveRatio(true);
        return iv;
    }
}
