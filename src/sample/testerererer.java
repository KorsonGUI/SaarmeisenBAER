package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;

public class testerererer extends Application {




    @Override
    public void start(Stage primaryStage) throws Exception {
        double max = 100;
        double half = max/2;
        double third = max/3;
        double twothirds = 2*max/3;
        double[] points = { half, 0.0,
                            max, third,
                            max, twothirds,
                            half, max,
                            0.0, twothirds,
                            0.0, third
        };
        Polygon hex = new Polygon(points);
        hex.setStyle("-fx-background-color: black");
        AnchorPane root = new AnchorPane();
        root.getChildren().add(hex);
        Scene primaryScene = new Scene(root, 1280, 720);
        primaryStage.setScene(primaryScene);
        primaryStage.show();

    }


}
