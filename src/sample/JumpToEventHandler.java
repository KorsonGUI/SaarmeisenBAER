package sample;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;


public class JumpToEventHandler implements EventHandler<MouseEvent> {

    private final TextField inputArea;
    private final WorldGUI gui;

    public JumpToEventHandler(WorldGUI gui, TextField area) {
        this.inputArea = area;
        this.gui = gui;
    }

    @Override
    public void handle(MouseEvent event) {
        try {
            int nextStep = Integer.parseInt(inputArea.getText());

            gui.setNextStep(nextStep);
            gui.update();
        } catch (NumberFormatException e) {
            gui.throwExceptionPopup(e);
        } catch (IllegalStateException e) {
            gui.throwEndOfSim();
        }
    }
}
