package sample;

import Implementations.MyField;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import saarland.cispa.sopra.systemtests.AntInfo;
import saarland.cispa.sopra.systemtests.FieldInfo;
import saarland.cispa.sopra.systemtests.GameInfo;
import saarland.cispa.sopra.systemtests.WorldInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class WorldGUI extends Application {

    static final String UNIBLUE = "-fx-background-color: rgb(16,66,128)";
    static final String MAINFONT = "Arial";
    private final String IconPath = "./resources/Icons/32x32.png";
    private final String Antpath = "./resources/Ants/";
    private final String Rockpath = "./resources/Rocks/";
    private final String Foodpath = "./resources/Food/";
    private final String Basepath = "./resources/Bases/";
    private final String AntLion = "./resources/AntLion/";
    //updatable components
    protected HBox infoBox;
    AnchorPane root;
    Stage primaryStage;
    private boolean colorFive = false;
    private String buttonStyleSheet = "-fx-text-fill: white;" + "-fx-font-family: Arial;" + "-fx-background-color: rgb(16,66,128);" + "-fx-border-color: black";
    private long l = 1;
    private File map = null;
    private File[] brains = null;
    private GameInfo game;
    private WorldInfo world;
    private int height;
    private int width;
    private int currentStep;
    private int nextStep = 0;
    private TextField jumpToArea;
    private Text currentStepLabel;
    private HBox scoreBoard;
    private List<Node> currentScores;
    private VBox fieldBox;
    private List<List<Button>> fields;
    private TextField speedDisplayArea;
    private int speed = 20;
    private Character swarmToDisplay = 'A';
    private boolean noSwarmDisplay = true;

    private FieldInfo currentField;

    private ReentrantLock runLock = new ReentrantLock();
    private boolean runSim;
    private int currentY;
    private int currentX;
    private Map<String, Image> antMap;
    private Map<Integer, Image> rockMap;
    private Map<String, Image> foodmap;
    private Image base;
    private Image antLion;
    private Scene primaryScene;
    private PannableCanvas pannableCanvas;
    private double m_nMouseX;
    private double m_nMouseY;
    private double m_nX;
    private double m_nY;
    private VBox infoControlPanel;


    public WorldGUI(GameInfo game, long l, File s, File... strings) {
        this.l = l;
        this.map = s;
        this.brains = strings;
        this.currentStep = 0;
        this.game = game;
        this.world = game.simulate(0, l, s, strings);
        antMap = new HashMap<>();
        File[] antpics = new File(Antpath).listFiles();
        for (File ant : antpics) {
            try {
                antMap.put(ant.getName(), new Image(new FileInputStream(ant)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        rockMap = new HashMap<>();
        File[] rockpics = new File(Rockpath).listFiles();
        for (int i = 0; i < rockpics.length; i++) {
            try {
                rockMap.put(i, new Image(new FileInputStream(rockpics[i])));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        foodmap = new HashMap<>();
        File[] foodpics = new File(Foodpath).listFiles();
        for (int i = 0; i < foodpics.length; i++) {
            try {
                foodmap.put(foodpics[i].getName(), new Image(new FileInputStream(foodpics[i])));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        {
            try {
                base = new Image(new FileInputStream(new File(Basepath + "base_round.png")));
                antLion = new Image(new FileInputStream(new File(AntLion + "antLion.png")));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    public static void main(String[] args) {
        launch(args);
    }

    static void setBorderStandard(Pane pane) {
        pane.setBorder(new Border(
                new BorderStroke(Color.BLACK,
                        BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0.2))));
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.root = new AnchorPane();
        this.primaryStage = primaryStage;
//        this.world = new MyWorld("./resources/Maps/Test.txt");
        height = world.getHeight();
        width = world.getWidth();
        root.getChildren().add(getMainWindow());
        Scene primaryScene = new Scene(root, 1280, 720);
        SceneGestures sceneGestures = new SceneGestures(pannableCanvas);
        primaryScene.addEventFilter(ScrollEvent.ANY, sceneGestures.getOnScrollEventHandler());
        primaryStage.getIcons().add(new Image(new FileInputStream(new File(IconPath))));
        primaryStage.setTitle("SaarmeisenBAER - Saarmeisen Bebilderung Auch Eventuell Richtig!");
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    public WorldInfo getWorld() {
        return this.world;
    }

    public int getCurrentStep() {
        return this.currentStep;
    }

//    public void updateInfos() {
//
//    }

    public void setNextStep(int i) {
        this.nextStep = Math.max(i, 0);
    }

    public void update() {
        currentStep = nextStep;
        try {
            this.world = game.simulate(currentStep, l, map, brains);
        } catch (IllegalStateException ex) {
            throwEndOfSim();
        }
        currentStepLabel.setText(new StringBuilder().append("Current step is: ").append(currentStep).toString());
        jumpToArea.setText("");
        updateScores();
        updateMap();
        updateInformationpanel(this.infoBox);
//        this.infoBox.getChildren().clear();
    }

    private void updateScores() {
        inputScores(this.scoreBoard);
    }

    private void inputScores(HBox hb) {
        hb.getChildren().clear();
        setBorderStandard(hb);
        hb.setAlignment(Pos.CENTER);
        hb.setPrefSize(560, 200);
        hb.setStyle("-fx-background-color: white");
        int score = 0;
        int ants = 0;
        List<Node> scores = new ArrayList<>();
        loop:
        for (int i = 0; i < 26; i++) {
            int ascii = i + 65;
            char swarm = (char) ascii;
            try {
                score = world.getScore(swarm);
                ants = getLivingAns(swarm);
                scores.add(getScorePanel(swarm, score, ants));
            } catch (Exception e) {
                break loop;
            }
        }
        hb.getChildren().addAll(scores);
        this.currentScores = scores;
    }

    private int getLivingAns(char swarm) {
        int count = 0;
        for (AntInfo ant : world.getAnts()) {
            if (ant.getSwarm() == swarm) {
                count++;
            }
        }
        return count;
    }


    private void updateMap() {
        updateFields(this.fieldBox);
    }

    private void updateFields(VBox vb) {
        List<List<Node>> rows = getNodeRows();
        vb.getChildren().clear();

        vb.setPrefSize(720, 720);
        for (List<Node> row : rows) {
            HBox hb = new HBox();
            hb.setPrefHeight(1);
            hb.setStyle(UNIBLUE);
            hb.setAlignment(Pos.TOP_CENTER);
            hb.setPrefSize(720 / height, 720);
            for (Iterator<Node> iter = row.iterator(); iter.hasNext(); ) {
                hb.getChildren().add(iter.next());
            }
            vb.getChildren().add(hb);
        }
//        HBox hb = new HBox();
//        hb.setPrefSize(720,720);
//        hb.setStyle("-fx-background-color: dodgerblue");
//        vb.getChildren().add(hb);
    }

    private List<List<Node>> getNodeRows() {
        double filling = 720 / Math.max(width, height) / 2;
        double prefWidth_Height = 720 / Math.max(width, height);
        boolean even = true;
        List<List<Node>> rows = new ArrayList<>(height);
        for (int y = 0; y < height; y++) {
            List<Node> row = new ArrayList<>(width);
            if (!even) {
                addFillerNode(row, filling, prefWidth_Height, y);
            }
            for (int x = 0; x < width; x++) {
                FieldInfo field = world.getFieldAt(x, y);
                Node node = getFieldView(field, prefWidth_Height);
                row.add(node);
            }
            if (even) {
                addFillerNode(row, filling, prefWidth_Height, y);
            }
            rows.add(row);
            even = !even;
        }
        return rows;
    }

    private Node getFieldView(FieldInfo field, double prefWidth_Height) {
        StackPane sp = new StackPane();
        sp.setAlignment(Pos.CENTER);
        sp.setPrefSize(prefWidth_Height, prefWidth_Height);
        if (field.getAnt().isPresent()) {
            setAntView(sp, field.getAnt().get(), prefWidth_Height, field);
            setUsableFunctions(sp, prefWidth_Height, field);
            return sp;
        }
        switch (field.getType()) {
            case '=':
                setAntLionView(sp, prefWidth_Height, field);
                break;
            case '.':
                if (field.getFood() > 0) {
                    setFoodView(sp, prefWidth_Height, field);
                    break;
                } else {
                    String str = getDisplayString(field);
                    HBox hb = new HBox();
                    setColorCode(hb, field);
                    setBorderStandard(hb);
                    hb.setPrefSize(prefWidth_Height, prefWidth_Height);
                    hb.setOnMouseClicked(new DisplayInformationEventHandler(this, field.getX(), field.getY()));
                    hb.setAlignment(Pos.CENTER);

                    Text txt = new Text(str);
                    txt.setFont(Font.font(MAINFONT));
                    txt.setStyle("-fx-font-size: " + prefWidth_Height / 3);
//                txt.setStyle("-fx-border-color: dodgerblue");
                    hb.getChildren().add(txt);
                    sp.getChildren().add(hb);
                    if (!noSwarmDisplay) {
                        sp.getChildren().add(getMarkerBox(prefWidth_Height, field, swarmToDisplay));
                    }
                    return sp;
                }
            case '#':
                setRockView(sp, prefWidth_Height, field);
                break;
            default:
                setBaseView(sp, prefWidth_Height, field);
        }
        setUsableFunctions(sp, prefWidth_Height, field);
//        setMarkers(sp, prefWidth_Height, field);
        return sp;
    }


    private Node getMarkerBox(double prefWidth_height, FieldInfo field, Character key) {
        boolean[] markers = field.getMarkers().get(key);
        double size = prefWidth_height * 2 / 3;
        StackPane outerBox = new StackPane();
        setUsableFunctions(outerBox, prefWidth_height, field);
        outerBox.setPrefSize(size, size);
        outerBox.setMinSize(size, size);
        outerBox.setMaxSize(size, size);
        outerBox.setBackground(Background.EMPTY);
        outerBox.setAlignment(Pos.CENTER);
        for (int i = 0; i < markers.length; i++) {
            HBox hb = new HBox();
            double scale = size * (i + 1) / 7;
            if (markers[i])
                outerBox.getChildren().add(makeMarkerDisplay(hb, i, scale));
        }
        return outerBox;
    }

    private Node makeMarkerDisplay(HBox hb, int i, double scale) {
        hb.setBackground(Background.EMPTY);
        setBorderStandard(hb);
        hb.setStyle("-fx-border-color: " + getColorForMarker(i));
        hb.setPrefSize(scale, scale);
        hb.setMinSize(scale, scale);
        hb.setMaxSize(scale, scale);
        return hb;
    }

    private String getColorForMarker(int i) {
        var r = 0;
        var g = 0;
        var b = 0;
        switch (i) {
            case 0:
                r = 175;
                g = 198;
                b = 255;
                break;
            case 1:
                r = 142;
                g = 175;
                b = 255;
                break;
            case 2:
                r = 107;
                g = 150;
                b = 255;
                break;
            case 3:
                r = 76;
                g = 128;
                b = 255;
                break;
            case 4:
                r = 22;
                g = 90;
                b = 255;
                break;
            case 5:
                r = 0;
                g = 45;
                b = 155;
                break;
            case 6:
                r = 0;
                g = 25;
                b = 89;
                break;
        }
        return getRGB(r, g, b);
    }

    private double[] getHexPoints(double max) {
        double half = max / 2;
        double third = max / 3;
        double twothirds = 2 * max / 3;
        double[] points = {half, 0.0,
                max, third,
                max, twothirds,
                half, max,
                0.0, twothirds,
                0.0, third
        };
        return points;
    }


//    private Node getFieldView(FieldInfo field, double prefWidth_Height) {
//        StackPane sp = new StackPane();
//        sp.setPrefSize(prefWidth_Height, prefWidth_Height);
//        if (field.getAnt().isPresent()) {
//            setAntView(sp, field.getAnt().get(), prefWidth_Height, field);
//            setUsableFunctions(sp, prefWidth_Height, field);
//            return sp;
//        }
//        switch (field.getType()) {
//            case '=':
//                setAntLionView(sp, prefWidth_Height, field);
//                setUsableFunctions(sp, prefWidth_Height, field);
//                return sp;
//            case '.':
//                if (field.getFood() > 0) {
//                    setFoodView(sp, prefWidth_Height, field);
//                    setUsableFunctions(sp, prefWidth_Height, field);
//                    return sp;
//                }
//                break;
//            case '#':
//                setRockView(sp, prefWidth_Height, field);
//                setUsableFunctions(sp, prefWidth_Height, field);
//                return sp;
//            default:
//                setBaseView(sp, prefWidth_Height, field);
//                setUsableFunctions(sp, prefWidth_Height, field);
//                return sp;
//        }
//        String str = getDisplayString(field);
//        HBox hb = new HBox();
//        setColorCode(hb, field);
//        setBorderStandard(hb);
//        hb.setPrefSize(prefWidth_Height, prefWidth_Height);
//        hb.setOnMouseClicked(new DisplayInformationEventHandler(this, field.getX(), field.getY()));
//        hb.setAlignment(Pos.CENTER);
//
//        Text txt = new Text(str);
//        txt.setFont(Font.font(MAINFONT));
//        txt.setStyle("-fx-font-size: " + prefWidth_Height / 3);
////                txt.setStyle("-fx-border-color: dodgerblue");
//        hb.getChildren().add(txt);
//        return hb;
//    }

    private void setAntLionView(StackPane sp, double prefWidth_height, FieldInfo field) {
        ImageView iv = new ImageView(antLion);
        setIVUseful(iv, prefWidth_height);
        HBox hb = getCorrectFieldHBox(field, prefWidth_height);
        hb.getChildren().add(iv);
        sp.getChildren().add(hb);
    }

    private void setBaseView(StackPane sp, double prefWidth_height, FieldInfo field) {
//        ImageView iv = new ImageView(base);
//        setIVUseful(iv, prefWidth_height);
        HBox hb = getCorrectFieldHBox(field, prefWidth_height);
        Label swarm = new Label(String.valueOf(field.getType()));
        swarm.setStyle("-fx-text-fill: white; -fx-font-size: " + prefWidth_height / 2);
//        hb.getChildren().add(swarm);
        if (!noSwarmDisplay) {
            hb.getChildren().add(getMarkerBox(prefWidth_height, field, swarmToDisplay));
        }
        sp.getChildren().add(hb);
        sp.getChildren().add(swarm);
    }

    private void setFoodView(StackPane sp, double prefWidth_height, FieldInfo field) {
        int food = field.getFood();
        if (food == 0) {
            return;
        }
        if (food >= 10) {
            food = 10;
        }
        ImageView iv = new ImageView(foodmap.get("food_" + food + ".png"));
        setIVUseful(iv, prefWidth_height);
        HBox hb = getCorrectFieldHBox(field, prefWidth_height);
//        hb.getChildren().add(iv);
        if (!noSwarmDisplay) {
            hb.getChildren().add(getMarkerBox(prefWidth_height, field, swarmToDisplay));
        }
        sp.getChildren().add(hb);
        sp.getChildren().add(iv);

    }

    private void setRockView(StackPane sp, double prefWidth_height, FieldInfo field) {
        ImageView iv = new ImageView(rockMap.get(0));
        setIVUseful(iv, prefWidth_height);
        HBox hb = getCorrectFieldHBox(field, prefWidth_height);
        hb.getChildren().add(iv);
        sp.getChildren().add(hb);
    }

    private HBox getCorrectFieldHBox(FieldInfo field, double prefheight) {
        HBox hb = new HBox();
        setColorCode(hb, field);
        hb.setAlignment(Pos.CENTER);
        hb.setPrefSize(prefheight, prefheight);
        setBorderStandard(hb);
        return hb;
    }

    private void setIVUseful(ImageView iv, double prefWidth_height) {
        iv.setFitWidth(prefWidth_height * 0.9);
        iv.setFitHeight(prefWidth_height * 0.9);
    }

    private void setUsableFunctions(StackPane sp, double prefWidth_height, FieldInfo field) {
        sp.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
            this.setCurrentX(field.getX());
            this.setCurrentY(field.getY());
            this.infoBox.getChildren().clear();
            this.infoBox.getChildren().add(getFieldInformation(field.getX(), field.getY()));
            this.infoBox.getChildren().add(getAntOnFieldInformation(field.getX(), field.getY()));
            event.consume();
        });
//        sp.setOnMouseClicked(new DisplayInformationEventHandler(this, field.getX(), field.getY()));
        sp.setPrefSize(prefWidth_height, prefWidth_height);
    }

    private Node getAntOnFieldInformation(int x, int y) {
        WorldInfo world = getWorld();
        FieldInfo field = world.getFieldAt(x, y);
        Optional<AntInfo> ant = field.getAnt();
        AntInfoPanel antPanel = new AntInfoPanel(ant, false);
        return antPanel.getPanel();
    }

    private VBox getFieldInformation(int x, int y) {
        WorldInfo world = getWorld();
        FieldInfo field = world.getFieldAt(x, y);
        FieldInfoPanel fieldPanel = new FieldInfoPanel(field, false);
        return fieldPanel.getPanel();
    }

    private void setAntView(StackPane sp, AntInfo ant, double prefheight, FieldInfo field) {
//        sp.setAlignment(Pos.TOP_CENTER);
        ImageView iv = ant.hasFood() ? getAntIV("ant_" + ant.getDirection() + "_food.png") : getAntIV("ant_" + ant.getDirection() + ".png");
        setIVUseful(iv, prefheight);
        HBox hb = getCorrectFieldHBox(field, prefheight);
//        hb.getChildren().add(iv);
//        iv.fitWidthProperty().bind(sp.widthProperty());
//        iv.fitHeightProperty().bind(sp.heightProperty());
        HBox labels = new HBox();
        labels.setPrefSize(prefheight, Math.floor(prefheight / 4));
        Label swarmLabel = new Label(String.valueOf(ant.getSwarm()));
        swarmLabel.setStyle("-fx-font-size: " + Math.floor(prefheight / 4));
        Label foodLabel = new Label(ant.hasFood() ? "has Food" : "");
        foodLabel.setStyle("-fx-font-size: " + Math.floor(prefheight / 4));
//        sp.getChildren().add(hb);
        if (!noSwarmDisplay) {
            hb.getChildren().add(getMarkerBox(prefheight, field, swarmToDisplay));
        }
        StackPane labelPane = new StackPane();
        labelPane.setPrefSize(prefheight, prefheight);
        labelPane.setAlignment(Pos.TOP_CENTER);
        labelPane.getChildren().add(hb);
        labelPane.getChildren().add(swarmLabel);
        sp.getChildren().add(labelPane);
        sp.getChildren().add(iv);
    }

    private ImageView getAntIV(String name) {
        ImageView iv = new ImageView(antMap.get(name));
        return iv;
//        if( iv != null){
//            return iv;
//        }
//        try {
//            //TODO make transparent
//            Image img = new Image(new FileInputStream(new File(Antpath + name)));
//            iv = new ImageView(img);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        iv.setPreserveRatio(false);
//        this.antMap.put(name, iv);
//        return iv;
    }

    private void setColorCode(Node hb, FieldInfo field) {
//        if (field.getAnt().isPresent()) {
//            setAntColorCode(hb, field);
//            return;
//        }
        if (colorFive) {
            if (hasMarkerFive(field.getMarkers())) {
                hb.setStyle("-fx-background-color: rgb(252, 196, 30)");
                return;
            }
        }
        switch (field.getType()) {
            case '#':
                hb.setStyle("-fx-background-color: rgb(71, 39, 0)");
                break;
            case '.':
                setFoodGradient(hb, field);
                break;
            case '=':
                hb.setStyle("-fx-background-color: rgb(153, 30, 0)");
                break;
            default:
                setAntColorCode(hb, field, true);
        }
    }

    private boolean hasMarkerFive(Map<Character, boolean[]> markers) {
        for (boolean[] markerer : markers.values()) {
            if (markerer[5]) {
                return true;
            }
        }
        return false;
    }

    private void setAntColorCode(Node hb, FieldInfo field, boolean base) {

        int X = base ? field.getType() % 10 : field.getAnt().get().getSwarm() % 10;
        int r = 0;
        int g = 0;
        int b = 0;
        switch (X) {
            case 0:
                r = 0;
                g = 255;
                b = 127;
                break;
            case 1:
                r = 255;
                g = 127;
                b = 0;
                break;
            case 2:
                r = 255;
                g = 255;
                b = 0;
                break;
            case 3:
                r = 127;
                g = 255;
                b = 0;
                break;
            case 4:
                r = 0;
                g = 255;
                b = 0;
                break;
            case 5:
                r = 255;
                g = 0;
                b = 0;
                break;
            case 6:
                r = 0;
                g = 255;
                b = 255;
                break;
            case 7:
                r = 0;
                g = 127;
                b = 255;
                break;
            case 8:
                r = 0;
                g = 0;
                b = 255;
                break;
            case 9:
                r = 127;
                g = 0;
                b = 255;
                break;
        }
        hb.setStyle("-fx-background-color: " + getRGB(r == 0 ? r : r - 50, g == 0 ? g : g - 50, b == 0 ? b : b - 50));
    }

    private void setFoodGradient(Node hb, FieldInfo field) {
        int rgbvalMod = field.getFood();
        int r = 145 - rgbvalMod * 10;
        int g = 255 - rgbvalMod * 10;
        int b = 165 - rgbvalMod * 10;
        if (rgbvalMod == 0) {
            r = 255;
            g = 255;
            b = 255;
        }
        hb.setStyle("-fx-background-color: " + getRGB(r, g, b));
    }

    private String getRGB(double r, double g, double b) {
        StringBuilder str = new StringBuilder();

        return str.append("rgb(").append(r).append(',').append(g).append(',').append(b).append(')').toString();
    }

    private void addFillerNode(List<Node> row, double filling, double prefWidth_height, int y) {
        HBox filler = new HBox();
        filler.setPrefWidth(filling);
        filler.setPrefHeight(prefWidth_height);
        filler.setStyle(UNIBLUE);
//        filler.setOpacity(0);
        filler.setAlignment(Pos.CENTER);
        Label txt = new Label("" + y);
        txt.setStyle("-fx-text-fill: white; -fx-font-size: " + prefWidth_height / 3);
        filler.getChildren().add(txt);
        row.add(filler);
    }

    private String getDisplayString(FieldInfo field) {
        String str;
        if (field.getAnt().isPresent()) {
            str = new StringBuilder().append("Ant").append(field.getAnt().get().getId()).toString();
        } else {
            if (field.getFood() != 0)
                str = new StringBuilder().append(field.getFood()).toString();
            else {
                if (field.getType() != '.')
                    str = new StringBuilder().append(field.getType()).toString();
                else {
                    str = "";
                }
            }
        }
        return str;
    }

//    private List<List<Node>> getButtonRows() {
//        double filling = 720 / Math.max(width, height) / 2;
//        double prefWidth_Height = 720 / Math.max(width, height);
//        boolean even = true;
//        List<List<Node>> rows = new ArrayList<>(height);
//        for (int y = 0; y < height; y++) {
//            List<Node> row = new ArrayList<>(width);
//            if (!even) {
//                addFiller(row, filling, prefWidth_Height);
//            }
//            for (int x = 0; x < width; x++) {
//                FieldInfo field = world.getFieldAt(x, y);
//                String str = getDisplayString(field);
//                Button btn = new Button();
////                btn.setStyle("-fx-background-color: aliceblue");
//                btn.setText(str);
//                btn.setPrefSize(prefWidth_Height, prefWidth_Height);
//                btn.setOnMouseClicked(new DisplayInformationEventHandler(this, x, y));
//                row.add(btn);
//            }
//            if (even) {
//                addFiller(row, filling, prefWidth_Height);
//            }
//            rows.add(row);
//            even = !even;
//        }
//        return rows;
//    }

    private HBox getMainWindow() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER_LEFT);
        hb.getChildren().add(getInfoControlPanel());
        hb.getChildren().add(getFieldPanel());
        return hb;
    }

    private VBox getInfoControlPanel() {
        VBox vb = new VBox();
        this.infoControlPanel = vb;
        vb.setAlignment(Pos.TOP_CENTER);
        vb.getChildren().add(getScoreBoard());
        vb.getChildren().add(getControlPanel());
        vb.getChildren().add(getInfoPanel());
        vb.getChildren().add(getContactPanel());
        vb.setPrefSize(560, 720);
        vb.setStyle("-fx-background-color: black");
        return vb;
    }

    private HBox getInfoPanel() {
        HBox hb = new HBox();
        hb.setPrefSize(560, 600);
        hb.setStyle(UNIBLUE);
        this.infoBox = hb;
        addEmptyInformationPanel(hb);
        return hb;
    }

    private void addEmptyInformationPanel(HBox hb) {
        hb.getChildren().clear();
        FieldInfoPanel fPanel = new FieldInfoPanel(new MyField(0, 0, "+"), true);
        AntInfoPanel aPanel = new AntInfoPanel(Optional.empty(), true);
        hb.getChildren().add(fPanel.getPanel());
        hb.getChildren().add(aPanel.getPanel());
        return;
    }

    private void updateInformationpanel(HBox hb) {
        hb.getChildren().clear();
        FieldInfo f = world.getFieldAt(currentX, currentY);
        FieldInfoPanel fPanel = new FieldInfoPanel(f, false);
        AntInfoPanel aPanel = new AntInfoPanel(f.getAnt(), false);
        hb.getChildren().add(fPanel.getPanel());
        hb.getChildren().add(aPanel.getPanel());
        return;
    }


    private VBox getControlPanel() {
        VBox vb = new VBox();
        vb.setAlignment(Pos.TOP_CENTER);
        vb.getChildren().add(getCurrentStepPanel());
        vb.getChildren().add(getMainControlPanel());
        vb.getChildren().add(getVideoControlPanel());
        vb.setStyle(UNIBLUE);
        vb.setPrefSize(560, 120);
        return vb;
    }

    private Node getVideoControlPanel() {
        HBox hb = new HBox();
        hb.setStyle("-fx-background-color: white");
        hb.setPrefSize(560, 50);
        hb.setAlignment(Pos.CENTER);
        hb.getChildren().addAll(getControlButtons());
        return hb;

    }

    private List<Node> getControlButtons() {
        List<Node> list = new ArrayList<>();
//        list.add(getSpeedButton(-1));
        this.speedDisplayArea = getSpeedDisplayArea();
//        list.add(getPauseButton());
        list.add(getSpeedControlButton());
        list.add(speedDisplayArea);
        list.add(getPlayButton());
//        list.add(getSpeedButton(1));
        list.add(getColorMarkerButton());
        return list;
    }

    private Node getColorMarkerButton() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        TextField tf = new TextField();
        tf.setPrefWidth(50);
        tf.setOnKeyPressed((KeyEvent ev) -> {
            if (ev.getCode() == KeyCode.ENTER) {
                if (!"".equals(tf.getText())) {
                    noSwarmDisplay = false;
                    swarmToDisplay = tf.getText().toCharArray()[0];
                } else {
                    noSwarmDisplay = true;
                }
                update();
            }
        });
        Label lab = new Label("  Swarm to show Marker: ");
        lab.setStyle("-fx-text-fill: white");
        hb.getChildren().add(lab);
        hb.setStyle(buttonStyleSheet);
        hb.getChildren().add(tf);
        return hb;
    }

    private Button getPlayButton() {
        Button btn = new Button();
        btn.setText("Start");
        btn.setPrefSize(100, 20);
        btn.setStyle(buttonStyleSheet);
        btn.setOnMouseClicked((MouseEvent e) -> {
            runLock.lock();
            this.runSim = true;
            runLock.unlock();
            try {
                runSimulation();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        });
        return btn;
    }

    private void runSimulation() throws InterruptedException {
        try {
            runLock.lock();
            if (runSim) {
                nextStep += speed;
                update();
            }
        } finally {
            runLock.unlock();
        }
    }

    private Button getPauseButton() {
        Button btn = new Button();
        btn.setPrefSize(100, 20);
        btn.setText("Pause");
        btn.setStyle(buttonStyleSheet);
        btn.setOnMouseClicked((MouseEvent e) -> {
            runLock.lock();
            this.runSim = false;
            runLock.unlock();
        });
        return btn;
    }

    private Button getSpeedButton(int i) {
        Button btn = new Button();
        btn.setPrefSize(100, 20);
        btn.setText(i > 0 ? "Faster" : "Slower");
        btn.setStyle(buttonStyleSheet);
        btn.setOnMouseClicked((MouseEvent e) -> this.speed += i);
        return btn;
    }

    private Node getSpeedControlButton() throws NumberFormatException {
        Button btn = new Button();
        btn.setPrefSize(100, 20);
        btn.setText("Change Speed to:");
        btn.setStyle(buttonStyleSheet);
        btn.setOnMouseClicked((MouseEvent e) -> this.speed = Integer.parseInt(this.speedDisplayArea.getText()));
        return btn;
    }

    private TextField getSpeedDisplayArea() {
        TextField txt = new TextField();
//        txt.setMaxSize(20, 20);
        txt.setPrefSize(50, 20);
        return txt;
    }

//    private Node getSpeedButton(int i) {
//        Button btn = new Button();
//        btn.setText(i > 0 ? "Faster" : "Slower");
//        btn.setOnMouseClicked(new SpeedUpEventhandler(i, this));
//    }

    private HBox getMainControlPanel() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        setBorderStandard(hb);
        hb.setPrefSize(560, 200);
        hb.setMinHeight(100);

        hb.getChildren().add(getSkipButton("Backward 10", -10));
        hb.getChildren().add(getJumpToStepPanel());
        hb.getChildren().add(getJumpPopDown());
        hb.getChildren().add(getSkipButton("Forward 1", 1));
        hb.getChildren().add(getSkipButton("Forward 10", 10));
        hb.setStyle("-fx-background-color: white");
        return hb;
    }

    private Node getJumpPopDown() {
        MenuButton mbtn = new MenuButton();
        mbtn.setText("Jumps");
        mbtn.setStyle("-fx-text-fill: white");
        mbtn.getItems().add(getJumpButton(1000));
        mbtn.getItems().add(getJumpButton(5000));
        mbtn.getItems().add(getJumpButton(10000));
        mbtn.getItems().add(getJumpButton(20000));
        mbtn.getItems().add(getJumpButton(50000));
        mbtn.getItems().add(getJumpButton(100000));
        mbtn.getItems().add(getJumpButton(1000000));
        return mbtn;
    }

    private MenuItem getJumpButton(int i) {
        MenuItem mi = new MenuItem();
        mi.setText("" + i);
        mi.setOnAction(new JumpToHardEventHandler(this, i));
        return mi;
    }

    private Node getJumpToStepPanel() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
        Button btn = new Button("Jump to Step:");
        btn.setStyle(buttonStyleSheet);
        TextField area = new TextField();

        area.setPrefSize(50, 10);
        this.jumpToArea = area;
//        area.setMaxSize(50, 10);
        btn.setOnMouseClicked(new JumpToEventHandler(this, area));
        hb.getChildren().add(btn);
        hb.getChildren().add(area);
        return hb;
    }

    private Node getSkipButton(String text, int toSkip) {
        Button btn = new Button();
        btn.setOnMouseClicked(new SkipEventHandler(this, toSkip));
        btn.setPrefSize(100, 20);
        btn.setText(text);
        btn.setStyle(buttonStyleSheet);
//        btn.setStyle("-fx-background-image: ./Icons/rewindpng.png"");
        return btn;
    }

    private HBox getCurrentStepPanel() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);
//        hb.setStyle(UNIBLUE);
        Text stepLabel = new Text();
        this.currentStepLabel = stepLabel;

        stepLabel.setText(new StringBuilder().append("CurrentStep is: ").append(currentStep).toString());
        stepLabel.setStyle("-fx-font-size: 20");
        stepLabel.setFont(Font.font(MAINFONT));
        setBorderStandard(hb);
        hb.getChildren().add(stepLabel);
        hb.setStyle("-fx-background-color: white");
        hb.setPrefSize(560, 100);
        return hb;
    }

    private HBox getScoreBoard() {
        HBox hb = new HBox();
        inputScores(hb);
        this.scoreBoard = hb;
        return hb;
    }

    private Node getScorePanel(char swarm, int score, int ants) {
        HBox hb = new HBox();
//        setBorderStandard(hb);
        hb.setStyle("-fx-background-color: white");
        Text txt = new Text(new StringBuilder().append(" Swarm ").append(swarm).append(": ").append(score).append("/" + ants).append("  ").toString());
        txt.setFont(Font.font(MAINFONT));
        txt.setStyle("-fx-font-size: 20");
        hb.getChildren().add(txt);
        return hb;
    }

    private Node getFieldPanel() {
        HBox hb = new HBox();
        ScrollPane sp = new ScrollPane();
        hb.setAlignment(Pos.CENTER);
        PannableCanvas pannableCanvas = new PannableCanvas();
        VBox vb = new VBox();
        vb.setStyle(UNIBLUE);
        vb.setAlignment(Pos.TOP_CENTER);
        vb.getChildren().add(getMap());
        pannableCanvas.getChildren().add(vb);
        Group wrapper = new Group(pannableCanvas);
        this.pannableCanvas = pannableCanvas;
//        hb.getChildren().add(wrapper);
//        hb.setPrefSize(720, 720);
//        hb.setMaxSize(720, 720);
//        hb.setMinSize(720, 720);
//        return hb;
        sp.setContent(wrapper);
        sp.setPrefSize(720, 720);
        sp.setMaxSize(720, 720);
        sp.setMinSize(720, 720);
        return sp;
    }

    private Node getContactPanel() {
        VBox vb = new VBox();
        vb.setAlignment(Pos.TOP_CENTER);
        File file = new File("./resources/Icons/UniBar.png");
        Image image = new Image(file.toURI().toString());
        ImageView iv = new ImageView(image);
        iv.setFitWidth(560);
        iv.setPreserveRatio(true);
        vb.getChildren().add(iv);
        vb.setPrefWidth(560);
        vb.getChildren().add(getContactInfo());
        return vb;
    }

    private Node getContactInfo() {
        HBox hb = new HBox();
        Text txt = new Text("Feedback to s8chadam@gmail.com         ");
        txt.setFont(Font.font("Arial"));
        txt.setStyle("-fx-font-style: italic");
        txt.setStyle("-fx-font-size: 10");
        hb.getChildren().add(txt);
        hb.getChildren().add(getCopyEmailButton());
        hb.setStyle("-fx-background-color: white");
        hb.setAlignment(Pos.CENTER);
        return hb;
    }

    private Node getCopyEmailButton() {
        Button btn = new Button();
        btn.setText("Copy Email");
        btn.setOnMouseClicked(new CopyToClipBoardEventHandler("s8chadam@gmail.com"));
        return btn;
    }

    private VBox getMap() {
        VBox vb = new VBox();
        updateFields(vb);
        this.fieldBox = vb;
        return vb;
    }

//    private void addFiller(List<Node> row, double filling, double height) {
//        Button filler = new Button();
//        filler.setPrefWidth(filling);
//        filler.setPrefHeight(height);
//        filler.setStyle("-fx-background-color: dodgerblue");
//        filler.setOpacity(0);
//        row.add(filler);
//    }

    public void throwExceptionPopup(Exception e) {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        VBox dialogVbox = new VBox(20);
        TextArea ta = new TextArea(new StringBuilder().append(e.toString()).append("\n\n\nStacktrace:\n").append(getStackTrace(e)).toString());
        ta.setPrefRowCount(20);
        dialogVbox.getChildren().add(ta);
        Scene dialogScene = new Scene(dialogVbox, 600, 300);
        dialog.setScene(dialogScene);
        try {
            dialog.getIcons().add(new Image(new FileInputStream(new File(IconPath))));
        } catch (Exception ex) {
            e.printStackTrace();
        }
        dialog.show();

    }

    private String getStackTrace(Exception e) {
        StringBuilder str = new StringBuilder();
        for (StackTraceElement tr : e.getStackTrace()) {
            str.append('\n').append(tr.toString());
        }
        return str.toString();

    }

    public void throwEndOfSim() {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        VBox dialogVbox = new VBox(20);
        runLock.lock();
        this.runSim = false;
        runLock.unlock();
        Text ta = new Text("End of simulation reached!");
        dialogVbox.getChildren().add(ta);
        Scene dialogScene = new Scene(dialogVbox, 600, 300);
        dialog.setScene(dialogScene);
        dialog.show();
    }

    public void setCurrentY(int y) {
        this.currentY = y;
    }

    public void setCurrentX(int x) {
        this.currentX = x;
    }


    //    private List<Button> getControlButtons() {
//        List<Button> btns = new ArrayList<>(4);
//        btns.add(getBackwardsTenButton());
////        btns.add(getBackwardsOneButton());
////        btns.add(getForwardsTenButton());
////        btns.add(getForwardsOneButton());
//        return btns;
//    }


    private Node makeDraggable(final Node node) {
        final DragContext dragContext = new DragContext();
        final Group wrapGroup = new Group(node);

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {

                        // disable mouse events for all children
                        mouseEvent.consume();

                    }
                });

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_DRAGGED,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {

                        // disable mouse events for all children
                        mouseEvent.consume();

                    }
                });

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {

                        // remember initial mouse cursor coordinates
                        // and node position
                        dragContext.mouseAnchorX = mouseEvent.getX();
                        dragContext.mouseAnchorY = mouseEvent.getY();
                        dragContext.initialTranslateX =
                                node.getTranslateX();
                        dragContext.initialTranslateY =
                                node.getTranslateY();

                    }
                });

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_DRAGGED,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {

                        // shift node from its initial position by delta
                        // calculated from mouse cursor movement
                        node.setTranslateX(
                                dragContext.initialTranslateX
                                        + mouseEvent.getX()
                                        - dragContext.mouseAnchorX);
                        node.setTranslateY(
                                dragContext.initialTranslateY
                                        + mouseEvent.getY()
                                        - dragContext.mouseAnchorY);

                    }
                });

        return wrapGroup;
    }

    private static final class DragContext {
        public double mouseAnchorX;
        public double mouseAnchorY;
        public double initialTranslateX;
        public double initialTranslateY;
    }

}
