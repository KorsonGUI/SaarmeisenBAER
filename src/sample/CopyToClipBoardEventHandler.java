package sample;

import javafx.event.EventHandler;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseEvent;

public class CopyToClipBoardEventHandler implements EventHandler<MouseEvent> {
    String toCopy;

    public CopyToClipBoardEventHandler(String s) {
        toCopy = s;
    }

    @Override
    public void handle(MouseEvent event) {
        final Clipboard clipboard = Clipboard.getSystemClipboard();
        final ClipboardContent content = new ClipboardContent();
        content.putString(toCopy);
        clipboard.setContent(content);
    }
}
