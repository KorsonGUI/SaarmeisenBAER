package Implementations;


import saarland.cispa.sopra.systemtests.AntInfo;
import saarland.cispa.sopra.systemtests.FieldInfo;

public class MyAnt implements AntInfo {
    private FieldInfo currentField;

    public MyAnt(FieldInfo field){
        currentField = field;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public boolean hasFood() {
        return false;
    }

    @Override
    public int getRestTime() {
        return 3;
    }

    @Override
    public boolean[] getRegister() {
        return new boolean[0];
    }

    @Override
    public int getPc() {
        return 2345;
    }

    @Override
    public FieldInfo getField() {
        return this.currentField;
    }

    @Override
    public char getSwarm() {
        return 'A';
    }

    @Override
    public String getDirection() {
        return "southwest";
    }
}
