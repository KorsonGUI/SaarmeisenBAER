package Implementations;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class MapParser {

    int width;
    int height;
    List<String> rows = new ArrayList<>();

    public MapParser(String Path) {


        try {
            File file = new File(Path);
            FileInputStream fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();
            String raw = new String(data, "UTF-8");
            String[] lines = raw.split("\r\n");
            this.width = Integer.parseInt(lines[0]);
            this.height = Integer.parseInt(lines[1]);
            for (int i = 2; i < lines.length; i++) {
                rows.add(lines[i].trim());
            }
            rows.remove("");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public MapMatrix getMap() {
        MapMatrix mM = new MapMatrix(width,height);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                mM.setField(new MyField(x, y, rows.get(y).substring(x, x + 1)));
            }
        }
        return mM;
    }
}
