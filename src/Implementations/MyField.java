package Implementations;



import saarland.cispa.sopra.systemtests.AntInfo;
import saarland.cispa.sopra.systemtests.FieldInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MyField implements FieldInfo {


    String val;
    int food;
    int x;
    int y;
    Optional<AntInfo> ant = Optional.empty();


    public MyField(int x, int y, String val) {
        this.x = x;
        this.y = y;
        this.val = val;
        try {
            this.food = Integer.parseInt(val);
        } catch (NumberFormatException e) {
        }
    }

    public void setAnt(AntInfo anti){
        this.ant = Optional.of(anti);
    }

    @Override
    public Map<Character, boolean[]> getMarkers() {
        return new HashMap<Character, boolean[]>();
    }

    @Override
    public int getFood() {
        return this.food;
    }

    @Override
    public Optional<AntInfo> getAnt() {
        return this.ant;
    }

    @Override
    public char getType() {
        return val.toCharArray()[0];
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }
}
