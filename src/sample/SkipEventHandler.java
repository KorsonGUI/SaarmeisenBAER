package sample;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class SkipEventHandler implements EventHandler<MouseEvent> {

    private final WorldGUI gui;
    private final int toStep;

    public SkipEventHandler(WorldGUI gui, int toStep){
        this.gui = gui;
        this.toStep = toStep;
    }

    @Override
    public void handle(MouseEvent event) {
        gui.setNextStep(gui.getCurrentStep() + toStep);
        gui.update();
    }

}
